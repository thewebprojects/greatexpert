<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'greatexpert' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-!s~+^/TRteU$fMh~+8mq|wh^6p}3-2~MNrp$MGvj3K6q|f;Sd^iX]!]jfprl<jU' );
define( 'SECURE_AUTH_KEY',  'lkmMmDu:e:O-}A,nnZGH=_CsH(hO2DHX7uOy=rlaP]e&[:0+6qh-QZ`K#-a)`Duy' );
define( 'LOGGED_IN_KEY',    'a36_M.tk^f*_i9K0yJ,q)[>UqD9:/[HGy|;qr]o96{b|4<N@P06:s 3I(=lx@/7X' );
define( 'NONCE_KEY',        'J7[66Xt.ft|Q6z98/iK@o[J[7eOF%EuSsn%crEu&K:eeusW58_^xAYd,|KKxEvS~' );
define( 'AUTH_SALT',        'uLXT/$& +C9[)%OL6ov7&0_P;<j`.$Vb9qo=uha>ia>$fu:0/k ,Qb}?]W_maIx?' );
define( 'SECURE_AUTH_SALT', 'X@vI;Dt(]35lD1LSmt}<0b,3j_-h#n-o@r@B~RR`lom9Q)]tyq.kz9WOQ#.%EgoN' );
define( 'LOGGED_IN_SALT',   'W6:tYA%Pa&ab*,BeDiJS!]OnB7CCEby{i5QOt<y^u5$B[%!LsZ(dh?QD_1j=BHaD' );
define( 'NONCE_SALT',       'j7[bfpI?&,_vY$qL,=9Al/8T!x88iu;wdU^_tA`r<)bAzRvIV}`C>jfL-J}|bLEB' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
