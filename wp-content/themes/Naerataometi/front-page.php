<?php
/* Template Name: Home*/
get_header();
?>

<!---->
<!--    <section class="home_1">-->
<!--        <div class="home_1_size">-->
<!--            <div class="home_1_left" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">-->
<!--                <h3>Lorem ipsum dolor sit<br>-->
<!--                    ametconsectetur adipiscing elit</h3>-->
<!--                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>-->
<!--                <button type="button" name="button">Register now</button>-->
<!--            </div>-->
<!--            <div class="home_1_right">-->
<!--                <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/header_bg.png" alt="">-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section class="home_2">-->
<!--        <div class="home_2_size">-->
<!--            <div class="home_2_left" data-aos="zoom-in-up">-->
<!--                --><?php //echo get_the_post_thumbnail( 27, array( 280, 190) );    ;?>
<!--            </div>-->
<!--            <div class="home_2_right" data-aos="zoom-in-up">-->
<!--                <h4> --><?php //echo get_the_title(27);?><!--</h4>-->
<!--                <p> --><?php //echo get_post_field('post_content', 27);?><!--</p>-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="home_2_size">-->
<!--            <div class="home_2_left" data-aos="zoom-in-up">-->
<!--                <h4> --><?php //echo get_the_title(30);?><!--</h4>-->
<!--                <p> --><?php //echo get_post_field('post_content', 30);?><!--</p>-->
<!---->
<!--            </div>-->
<!--            <div class="home_2_right img_right" data-aos="zoom-in-up">-->
<!--                --><?php //echo get_the_post_thumbnail( 30, array( 280, 190) );    ;?>
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section class="home_3">-->
<!--        <div class="opacity_bg">-->
<!--            <h4>--><?php //echo get_the_title(33);?><!--</h4>-->
<!--            <div class="border_bottom">-->
<!---->
<!--            </div>-->
<!--            <p class="home_3_text">--><?php //echo get_post_field('post_content', 33);?><!--</p>-->
<!--            <div class="home_3_size">-->
<!--                <div class="home_3_box" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">-->
<!--                    --><?php
//                    $img_1=get_field('image_1','33');
//                    $img_2=get_field('image_2','33');
//                    $img_3=get_field('image_3','33');
//                    $img_4=get_field('image_4','33');
//                    ?>
<!--                    <img src="--><?php //echo $img_1['url']; ?><!--" alt="">-->
<!--                    <h6>--><?//=get_field('title_1','33')?><!--</h6>-->
<!--                    <p>--><?//=get_field('text_1','33')?><!--</p>-->
<!--                </div>-->
<!--                <div class="home_3_box" data-aos="fade-down"-->
<!--                     data-aos-easing="linear"-->
<!--                     data-aos-duration="1500">-->
<!--                    <img src="--><?php //echo $img_2['url'];?><!--" alt="">-->
<!--                    <h6>--><?//=get_field('title_2','33')?><!-- </h6>-->
<!--                    <p>--><?//=get_field('text_2','33')?><!--</p>-->
<!--                </div>-->
<!--                <div class="home_3_box" data-aos="fade-down"-->
<!--                     data-aos-easing="linear"-->
<!--                     data-aos-duration="1500">-->
<!--                    <img src="--><?php //echo $img_3['url']; ?><!--" alt="">-->
<!--                    <h6>--><?//=get_field('title_3','33')?><!-- </h6>-->
<!--                    <p>--><?//=get_field('text_3','33')?><!--</p>-->
<!--                </div>-->
<!--                <div class="home_3_box" data-aos="fade-down"-->
<!--                     data-aos-easing="linear"-->
<!--                     data-aos-duration="1500">-->
<!--                    <img src="--><?php //echo $img_4['url']; ?><!--" alt="">-->
<!--                    <h6>--><?//=get_field('title_4','33')?><!-- </h6>-->
<!--                    <p>--><?//=get_field('text_4','33')?><!--</p>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section class="home_4">-->
<!--<style>-->
<!--    .home_4_size{-->
<!--        counter-reset: section;-->
<!--    }-->
<!--    .home_nummber::before {-->
<!--        counter-increment: section;-->
<!--        content:counter(section);-->
<!--    }-->
<!--</style>-->
<!--        <h3>--><?php //echo get_cat_name(3); ?><!--</h3>-->
<!--        <div class="home_4_text">--><?php //echo category_description(3); ?><!-- </div>-->
<!--        <div class="home_4_size">-->
<!--            --><?php
//            $posts = get_posts(array('post_type'=>'', 'category'=>'3'));
////            var_dump($posts);
//            foreach ($posts as $post){
////                echo $post->post_name;
////                echo $post->post_excerpt;
////                echo $post->guid;
//                ?>
<!--                <div class="home_4_box" data-aos="zoom-out">-->
<!---->
<!--                        <div class="home_4_poss">-->
<!--                            <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/logo.png" alt="">-->
<!--                            <p class="home_nummber"></p>-->
<!--                        </div>-->
<!--                         <p class="home_4_box_text">--><?php //echo $post->post_name;?><!-- </p>-->
<!--                        <p class="home_4_box_text2">--><?php //echo $post->post_excerpt;?><!-- </p>-->
<!---->
<!---->
<!--                </div>-->
<!---->
<!--                --><?php
//            }
//            ?>
<!--        </div>-->
<!--    </section>-->
<!--    <section class="home_5">-->
<!--        <div class="opacity_bg">-->
<!--            <h5>Happy clients</h5>-->
<!--            <div class="home_5_poss">-->
<!--                <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/logo.png" alt="" class="logo_img">-->
<!--                <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/marks.png" alt="" class="marks">-->
<!--            </div>-->
<!--            <div class="owl-carousel owl-theme">-->
<!--            --><?php
//            $testimonials = get_posts(array('post_type'=>'', 'category'=>'4'));
////            var_dump($testimonials);
//                foreach ($testimonials as $testimonial){
//
//            ?>
<!--                <div class="item"><h4>--><?php //echo $testimonial->post_content;?><!--</h4><p>--><?php //echo $testimonial->post_name;?><!--</p></div>-->
<!--                --><?php //}?>
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->
<!--    <section class="home_6">-->
<!--        <div class="home_6_left" data-aos="flip-left"-->
<!--             data-aos-easing="ease-out-cubic"-->
<!--             data-aos-duration="2000">-->
<!--            <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/img5.png" alt="">-->
<!--        </div>-->
<!--        <div class="home_6_right" data-aos="flip-left"-->
<!--             data-aos-easing="ease-out-cubic"-->
<!--             data-aos-duration="2000">-->
<!--                  --><?php //echo do_shortcode('[mc4wp_form id="75"]');?>
<!--        </div>-->
<!--    </section>-->




    <section class="h-section1">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Aitame puudustes peresid -ja
                        lapsi üle-Eesti. Aita meil aidata :)</h2>
                    <div class="select-box">
                        <div class="dropdown">
                            <button class="dropbtn"><img src="<?php echo get_template_directory_uri(); ?>/images/heart.png"> anneta
                                <i class="fa fa-caret-down"></i>
                            </button>
                            <div class="dropdown-content">
                                <a href="#">Anneta asju</a>
                                <a href="#">Toeta ettevõttena</a>
                                <a href="#">Olen vabatahtlik</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
    </section>

    <section class="h-section2">
        <div class="container">
            <div class="h-section2-1">
                <div class="row">
                    <?php
                                $posts = get_posts(array('post_type'=>'', 'category'=>'3'));
//                                var_dump($posts);
                                foreach ($posts as $post){
                    //                echo $post->post_name;
                    //                echo $post->post_excerpt;
                    //                echo $post->guid;
                                    ?>

                                    <div class="col-md-3">
                                        <div class="post-box">
                                           <?php echo get_the_post_thumbnail( $post->ID);?>

                                            <a href="<?php echo  $post->guid;?>">
                                                <h1><?php echo $post->post_name;?>
                                                </h1>
                                            </a>
                                            <p>
                                                <?php echo $post->post_excerpt;?>
                                            </p>
                                            <div class="post_range-slider">
                                                <p>500€ <br>donated</p>
                                                <input type="range" min="1" max="100" value="33" class="slider" id="myRange1">
                                                <div class="total-price">1500€</div>
                                            </div>
                                            <div class="post-box-btn">
                                                <a href="">tahan aidata<img src="<?php echo get_template_directory_uri(); ?>/images/heart-small.png"> </a>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                }
                                ?>


                </div>
                <div class="see_all_btn">
                    <a href="#">Vaata rohkem</a>
                </div>
            </div>

            <div class="h-section2-1">
                <div class="row">

                    <div class="col-md-8">
                        <div class="h-sectn2-1-left">
                            <div class="h-sectn2-1-top">
                                <h2>Väiksed projektid</h2>
                                <div class="see-btn">
                                    <a href="#">Vaata kõik</a>
                                </div>
                            </div>
                            <div class="h-sectn2-1-bottom">
                                <?php
                                $posts = get_posts(array('post_type'=>'', 'category'=>'4'));
                                //                                var_dump($posts);
                                foreach ($posts as $post){
                                    //                echo $post->post_name;
                                    //                echo $post->post_excerpt;
                                    //                echo $post->guid;
                                    ?>
                                    <div class="h-sect2-1_box">
                                        <a href="<?php echo  $post->guid;?>">
                                            <?php echo get_the_post_thumbnail( $post->ID);?>
                                            <p><?php echo $post->post_name;?></p>
                                        </a>
                                    </div>


                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="h-sectn2-1-right">
                            <h3>Aita meil aidata :)</h3>
                            <div class="h-sect1-2_price">

                                <p>  <?= get_field('title_1','14');?></p>
                                <div class="h-sect1-2__price">
                                      <?= get_field('price_1','14');?>
                                </div>
                            </div>
                            <div class="h-sect1-2_price">
                                <p>  <?= get_field('title_2','14');?></p>
                                <div class="h-sect1-2__price">
                                    <?= get_field('price_2','14');?>
                                </div>
                            </div>
                            <div class="h-sect1-2_price">
                                <p>  <?= get_field('title_3','14');?></p>
                                <div class="h-sect1-2__price">
                                    <?= get_field('price_3','14');?>
                                </div>
                            </div>
                            <p>Elion, Elisa, EMT, Tele2, STV ja<br> Starman on teenustasudest<br> loobunud!</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>


    <section class="h-section3">
        <div class="container">

            <div class="h-section3-1">
                <h2>Meie Kallid sõbrad-toetajad! :)</h2>
                <div class="row">
                    <div class="col-md-8">
                        <div class="h-sectn3-1-left">
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/g1.png">
                                </a>
                            </div>
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/g2.png">
                                </a>
                            </div>
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/g3.png">
                                </a>
                            </div>
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/g4.png">
                                </a>
                            </div>
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/g5.png">
                                </a>
                            </div>
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/g6.png">
                                </a>
                            </div>
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/g7.png">
                                </a>
                            </div>
                            <div class="h-sect3-1_box">
                                <a href="#">
                                    +<br>
                                    Vaata<br>
                                    rohkem
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="h-sectn3-1-right">
                            <?php $img= get_field('image','14');?>
                            <img src="<?php echo $img['url']; ?>">
                        </div>
                    </div>
                </div>
            </div>

            <div class="h-section3-2">
                <div class="row">
                    <div class="col-md-8">
                        <div class="h-sectn3-2-left">
                            <div class="fb-title">Find us on Facebook</div>
                            <div class="fb-like">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
                                <div class="fb___like">Naerata Ometi<br>
                                    <div class="fb_like_btn>"><a href="#"> <i class="fab fa-facebook-square"></i>Like</a>
                                    </div>
                                </div>
                            </div>
                            <div class="fb-imgs">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer48.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer50.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer51.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer52.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer53.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer49.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer50.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer51.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer52.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer53.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer48.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer50.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer51.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer52.png">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/Layer53.png">
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </section>



<?php
get_footer();
?>