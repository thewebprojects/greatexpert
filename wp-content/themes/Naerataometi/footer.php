<?php

//$social_links =  get_field("social_links");

?>
<footer class="f_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="f_info">
                    <ul>
                        <?php if ( is_active_sidebar( 'footerwidget-2' ) ) : ?>
                            <?php dynamic_sidebar( 'footerwidget-2' ); ?>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="f_social">
                    <ul class="list-inline">
                        <?php if ( is_active_sidebar( 'footerwidget-1' ) ) : ?>
                            <?php dynamic_sidebar( 'footerwidget-1' ); ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>



</body>
</html>

<?php wp_footer(); ?>


