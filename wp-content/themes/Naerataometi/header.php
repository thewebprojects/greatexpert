<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Academy
 * @since 1.0.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<body>
<div class="top-banner">
    <nav class="nav_bar">
        <div class="container">
            <a class="navbar-logo" href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png"> </a>
            <button class="nav-bar navbar-btn" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i>
            </button>
            <div class="top-nav">

                <div class="nav-menu">
<!--                    <ul>-->
<!---->
<!--                        <li class="dropdown">-->
<!--                            <a href="" class="dropbtn" aria-disabled="true">Meist-->
<!--                                <i class="fa fa-caret-down"></i>-->
<!--                            </a>-->
<!--                            <div class="dropdown-content">-->
<!--                                <a href="#"><i class="fas fa-caret-right"></i>Typi non habent</a>-->
<!---->
<!--                            </div>-->
<!--                        </li>-->
<!--                        <li class="dropdown">-->
<!--                            <a href="" class="dropbtn" aria-disabled="true">Projektid-->
<!--                                <i class="fa fa-caret-down"></i>-->
<!--                            </a>-->
<!--                            <div class="dropdown-content">-->
<!--                                <a href="#"><i class="fas fa-caret-right"></i>Typi non habent</a>-->
<!--                                <a href="#"><i class="fas fa-caret-right"></i>Typi non habent</a>-->
<!--                                <a href="#"><i class="fas fa-caret-right"></i>Typi non habent</a>-->
<!--                                <a href="#"><i class="fas fa-caret-right"></i>Typi non habent</a>-->
<!--                            </div>-->
<!--                        </li>-->
<!--                        <li class="dropdown">-->
<!--                            <a href="" class="dropbtn" aria-disabled="true">Kuidas aidata?-->
<!--                                <i class="fa fa-caret-down"></i>-->
<!--                            </a>-->
<!--                            <div class="dropdown-content">-->
<!--                                <a href="#"><i class="fas fa-caret-right"></i>Typi non habent</a>-->
<!--                            </div>-->
<!--                        </li>-->
<!--                        <li><a href="">Oled abivajaja?</a></li>-->
<!--                        <li><a href="">Galerii</a></li>-->
<!--                        <li><a href="">Toetajad</a></li>-->
<!--                        <li><a href="">Kontakt</a></li>-->
<!---->
<!--                    </ul>-->
                    <?php
                    wp_nav_menu( array(
                        'theme_location'	=> 'primary',
                        'menu_id'			=> 'primary',
                        'menu_class'		=> 'navbar-nav',
                        'container' => 'ul',
                        'items_wrap' => '<ul id="%1$s" class="%2$s" role="menu" >%3$s</ul>',
//                'menu_class'=> 'nav navbar-nav navbar-right'

                    ) );
                    ?>
                </div>

            </div>

        </div>
    </nav>
</div>
<header class="header_fix">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <!-- <a class="navbar-brand" href="#">Hidden brand</a> -->

            <?php
            wp_nav_menu( array(
                'theme_location'	=> 'primary',
                'menu_id'			=> 'primary',
                'menu_class'		=> 'navbar-nav',
                'container' => 'ul',
                'items_wrap' => '<ul id="%1$s" class="%2$s" role="menu" >%3$s</ul>',
//                'menu_class'=> 'nav navbar-nav navbar-right'

            ) );
            ?>
<!--            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">-->
<!--                <li class="nav-item ">-->
<!--                    <a class="nav-link" href="#">Home</a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link" href="#">Jobs</a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link disabled" href="#">Events</a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link disabled" href="#">Forum</a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link disabled" href="#">Scholarship</a>-->
<!--                </li>-->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link disabled" href="#">Membership</a>-->
<!--                </li>-->
<!--            </ul>-->
            <form class="form-inline my-2 my-lg-0">
                <button class="btn  my-2 my-sm-0" type="submit"><a href="<?=site_url();?>/login/">SIgn in</a></button>
                <button class="btn my-2 my-sm-0" type="submit"><a href="<?=site_url();?>/register/">SIgn up</a></button>
            </form>
        </div>
    </nav>
</header>

