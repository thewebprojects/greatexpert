<?php
/* Template Name: Job*/
get_header();
?>

<section class="job_1">
    <div class="job_1_size">
        <h2>Your Higher Education <br> Job is Here</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has <br> been the industry's standard dummy text ever since the 1500s</p>
        <div class="job_1_inputs">
            <select class="" name="">
                <option value="">By location</option>
                <option value="">By location</option>
                <option value="">By location</option>
            </select>
            <select class="" name="">
                <option value="">By school</option>
                <option value="">By school</option>
                <option value="">By school</option>
            </select>
            <select class="" name="">
                <option value="">By type</option>
                <option value="">By type</option>
                <option value="">By type</option>
            </select>
            <button type="button" name="button">Search</button>
        </div>
    </div>
</section>
<section class="job_2">
    <div class="job_2_size">
        <h3>LOREM IPSUM</h3>
        <p class="job_2_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br>
            Lorem Ipsum has been the industry's standard dummy</p>
        <div class="job_2_flex">
            <div class="job_2_flex_left">
                <h5>Find your next job in <br> higher education</h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took </p>
                <div class="job_2_buttons">
                    <button type="button" name="button">Start now</button>
                </div>
            </div>
            <div class="job_2_flex_right">
                <h5>Post jobs and attract <br>  top talent</h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took </p>
                <div class="job_2_buttons">
                    <button type="button" name="button">Start now</button>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home_2">
    <div class="home_2_size">
        <div class="home_2_left" data-aos="zoom-in-up">
            <img src="img/img2.png" alt="">
        </div>
        <div class="home_2_right" data-aos="zoom-in-up">
            <h4>Create a job-seeker profile today</h4>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a
                type specimen book.</p>
            <button type="button" name="button">Create</button>
        </div>
    </div>

</section>
<section class="home_4">
    <style>
        .home_4_size{
            counter-reset: section;
        }
        .home_nummber::before {
            counter-increment: section;
            content:counter(section);
        }
    </style>
    <h3><?php echo get_cat_name(3); ?></h3>
    <div class="home_4_text"><?php echo category_description(3); ?> </div>
    <div class="home_4_size">
        <?php
        $posts = get_posts(array('post_type'=>'', 'category'=>'3'));
        //            var_dump($posts);
        foreach ($posts as $post){
//                echo $post->post_name;
//                echo $post->post_excerpt;
//                echo $post->guid;
            ?>
            <div class="home_4_box" data-aos="zoom-out">

                <div class="home_4_poss">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
                    <p class="home_nummber"></p>
                </div>
                <p class="home_4_box_text"><?php echo $post->post_name;?> </p>
                <p class="home_4_box_text2"><?php echo $post->post_excerpt;?> </p>


            </div>

            <?php
        }
        ?>
    </div>
</section>
<section class="home_6">
    <div class="home_6_left" data-aos="flip-left"
         data-aos-easing="ease-out-cubic"
         data-aos-duration="2000">
        <img src="<?php echo get_template_directory_uri(); ?>/img/img5.png" alt="">
    </div>
    <div class="home_6_right" data-aos="flip-left"
         data-aos-easing="ease-out-cubic"
         data-aos-duration="2000">
        <?php echo do_shortcode('[mc4wp_form id="75"]');?>
    </div>
</section>
<?php
get_footer();
?>
