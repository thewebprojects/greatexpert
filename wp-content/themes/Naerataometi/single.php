<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Academy
 * @since 1.0.0
 */

get_header();
//$post_header=get_field("post_header");
?>
<!--    <section class="single-header">-->
<!--        <h2>--><?//=$post_header['title']?><!--</h2>-->
<!--        <p> --><?//=$post_header['text']?><!--</p>-->
<!--    </section>-->
    <section id="primary" class="content-area">
    <main id="main" class="site-main">
    <div class="single-bg">
        <div class="single-page">
            <div class="single-news">
                <?php
                /* Start the Loop */
                while ( have_posts() ) :
                the_post();
                ?>
                <h1>  <?php the_title();?>  </h1>

                <?php the_content();
            ?>



<!--                <div class="news-shears">-->
<!--                    <div class="calendar">-->
<!--                        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/calendar%20(2).png">-->
<!--                        <span> --><?php //echo get_the_date('j-n-Y'); ?><!--</span>-->
<!--                    </div>-->
<!--                    <div class="shears">--><?php //echo do_shortcode('[ssba-buttons]');?>
<!--                        <span>SHARE:</span>-->
<!---->
<!--                        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/fb-single.png">-->
<!--                        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/in-single.png">-->
<!--                        <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/tw-single.png">-->
<!--                    </div>-->
<!---->
<!--                </div>-->
            </div>

            <?php  endwhile; // End of the loop.
            ?>


        </div>
    </div>
    </main>
    </section>


<?php
get_footer();
