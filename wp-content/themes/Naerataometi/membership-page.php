<?php
/* Template Name: membership*/
get_header();
?>
    <section id="primary" class="content-area">
    <main id="main" class="site-main">
<?php
$args = array(
    'post_type'      => 'product',
    'posts_per_page' => 10,
    'product_cat'    => ''
);

$loop = new WP_Query( $args );

while ( $loop->have_posts() ) : $loop->the_post();
    global $product;

    echo '<br /><a href="'.get_permalink().'">' . woocommerce_get_product_thumbnail().' <br />'.get_the_title().'</a><br />';
    echo '<br /><a href="'.get_permalink().'">$ '.$product->price.'</a><br />';?>

    <a href="<?php
    $add_to_cart = do_shortcode('[add_to_cart_url id="'.$product->ID.'"]');
    echo $add_to_cart;
    ?>" class="more">Buy now</a>
        <?php
endwhile;
//print_r($product);
//wp_reset_query();
?>
    </main>
    </section>

<?php
get_footer();
?>