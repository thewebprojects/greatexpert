<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Academy
 * @since 1.0.0
 */

get_header();
?>
<html>
    <body>


    <!------ Include the above in your HEAD tag ---------->
    <section class="header">
        <header>
            <nav class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#" data-toggle="dropdown" class="dropdown-toggle">Home<i class="fas fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Inbox</a></li>
                                <li><a href="#">Drafts</a></li>
                                <li><a href="#">Sent Items</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Trash</a></li>
                            </ul>
                        </li>
                        <li><a href="#" data-toggle="dropdown" class="dropdown-toggle">Upcoming Classes</a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Inbox</a></li>
                                <li><a href="#">Drafts</a></li>
                                <li><a href="#">Sent Items</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Trash</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Florida Security Training</a></li>
                        <li><a href="#">Firearm Courses </a></li>
                        <li><a href="#">Instructor Classes </a></li>
                        <li><a href="#"> Private Investigator Services</a></li>
                        <li><a href="#"> General & Misc</a></li>
                        <li><a href="#"> Contact</a></li>
                    </ul>

                </div><!-- /.navbar-collapse -->

            </nav>

        </header>

    </section>


    <section class="slide-wrapper">
        <div class="slide-block" style=""> <!-- wrap @img width -->

            <div id="carousel-example-generic" class="carousel slide"  data-ride="carousel" data-interval="3000">
                <!-- Indicators -->
                <ol class="carousel-indicators carousel-indicators-numbers"  style="    bottom: 50%; right: 0 !important;z-index: 99999;transform: rotate(90deg);margin: 0 !important;padding: 0 !important; width: auto; left: auto;">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner vertical" role="listbox">
                    <div class="item active">
                        <div class="slide-content" style="">
                            <img u="image" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>//img/Rectangle%202.jpg" alt="Alternate Text"
                                 height="300px" width="100%"/>
                            <div class="slide-info">
                                <h1>Firearms training academy</h1>
                                <div class="line"></div>
                                <p>Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit
                                    standard ända sedan 1500-talet, när en okänd boksättare tog att antal
                                    bokstäver och blandade dem för att göra ett provexemplar av en bok. </p>
                                <button>
                                    Start now
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="item ">
                        <div class="slide-content" style="">
                            <img u="image" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>//img/Rectangle%202.jpg" alt="Alternate Text"
                                 height="300px" width="100%"/>
                            <div class="slide-info">
                                <h1>Firearms training academy</h1>
                                <div class="line"></div>
                                <p>Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit
                                    standard ända sedan 1500-talet, när en okänd boksättare tog att antal
                                    bokstäver och blandade dem för att göra ett provexemplar av en bok. </p>
                                <button>
                                    Start now
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="item ">
                        <div class="slide-content" style="">
                            <img u="image" class="img-responsive" src="<?php echo get_template_directory_uri(); ?>//img/Rectangle%202.jpg" alt="Alternate Text"
                                 height="300px" width="100%"/>
                            <div class="slide-info">
                                <h1>Firearms training academy</h1>
                                <div class="line"></div>
                                <p>Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum har varit
                                    standard ända sedan 1500-talet, när en okänd boksättare tog att antal
                                    bokstäver och blandade dem för att göra ett provexemplar av en bok. </p>
                                <button>
                                    Start now
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Controls -->

            </div>
        </div>
    </section>

    <section class="about">
        <h1>About Us</h1>
        <div class="about-us">
            <div class="about-us-img">
                <img src="<?php echo get_template_directory_uri(); ?>/img/about_us.png">
            </div>
            <div class="about-us-cont">
                <p>
                    Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin.
                    Lorem ipsum har varit standard ända sedan 1500-talet, när en
                    okänd boksättare tog att antal bokstäver och blandade dem för
                    att göra ett provexemplar av en bok. </p><br>

                <div class="more-about">more <i class="fas fa-angle-double-right"></i></div>
            </div>
        </div>

    </section>

    <section class="we-do">
        <h1>What we do</h1>
        <div class="do-container">
            <div class="do-item">
                <h2>01</h2>
                <h3>Lorem ipsum</h3>
                <p>Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin.
                    Lorem ipsum har varit standard ända sedan 1500-talet, när en
                    okänd boksättare tog att antal</p>
            </div>
            <div class="do-item">
                <h2>02</h2>
                <h3>Lorem ipsum</h3>
                <p>Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin.
                    Lorem ipsum har varit standard ända sedan 1500-talet, när en
                    okänd boksättare tog att antal</p>
            </div>
            <div class="do-item">
                <h2>03</h2>
                <h3>Lorem ipsum</h3>
                <p>Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin.
                    Lorem ipsum har varit standard ända sedan 1500-talet, när en
                    okänd boksättare tog att antal</p>
            </div>
        </div>
    </section>

    <section class="needed">
        <h1>Why its needed</h1>
        <div class="needed-block">
            <div class="needed-cont">
                <div class="needed-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/icon.png"></span>Lorem Ipsum är en utfyllnadstext från tryck-
                    och förlagsindustrin.
                </div>
                <div class="needed-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/icon.png"></span>Lorem Ipsum är en utfyllnadstext från tryck-
                    och förlagsindustrin.
                </div>
                <div class="needed-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/icon.png"></span>Lorem Ipsum är en utfyllnadstext från tryck-
                    och förlagsindustrin.
                </div>
                <div class="needed-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/icon.png"></span>Lorem Ipsum är en utfyllnadstext från tryck-
                    och förlagsindustrin.
                </div>
                <div class="needed-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/icon.png"></span>Lorem Ipsum är en utfyllnadstext från tryck-
                    och förlagsindustrin.
                </div>
            </div>
            <div class="needed-img">
                <img src="./img/needed.png">
            </div>

        </div>

        <div class="courses">
            <div class="courses-block">
                <h2>Student Level Courses</h2>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="more">Show more</div>
            </div>
            <div class="courses-block">
                <h2>Instructor Level Courses</h2>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="courses-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/green-icon.png"></span>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin.
                </div>
                <div class="more">Show more</div>
            </div>
        </div>
    </section>
    <section class="security">
        <div class="security-block">
            <img src="./img/home.png">
        </div>
        <div class="security-block security-text">
            <h1>Home Security Surveys</h1>
            <div class="home-line"></div>
            <p>Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem
                ipsum har varit standard ända sedan 1500-talet, när en okänd boksättare tog
                att antal bokstäver och blandade dem för att göra ett provexemplar av en bok.
                Lorem Ipsum är en utfyllnadstext från tryck- och förlagsindustrin. Lorem ipsum
                har varit standard ända sedan 1500-talet, när en okänd boksättare tog att
                antal bokstäver och blandade dem för att göra ett provexemplar av en bok. </p>
        </div>
    </section>


    <section class="subscribe">
        <h1>Newsletter</h1>
        <p>Just hit the subscribe button and you will become my newsletter recipient, getting all the latest case studies,
            photos and news that I regularly
            post to my blog</p>
        <div class="form-block">
            <div class="input-group">
                <input type="email" class="form-control" placeholder="Enter your email">
                <span class="input-group-btn">
         <button class="btn" type="submit">Subscribe</button>
         </span>
            </div>
        </div>
    </section>

    <section class="footer">
        <div class="content">
            <div class=" footer-item fabout">
                <h1>About us</h1>
                <p>Lorem Ipsum är en utfyllnadstext från
                    tryck- och förlagsindustrin. Lorem
                    ipsum har varit standard ända sedan
                    1500-talet, när en okänd boksättare tog
                    att antal bokstäver och blandade dem
                    för att göra ett provexemplar av en bok. </p>
            </div>
            <div class="footer-item contacts">
                <h1>Contacts</h1>
                <div class="contacts-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/map.png"></span><p>
                        4096 N Highland St, Arlington, <br>VA
                        32101, USA
                    </p>
                </div>
                <div class="contacts-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/mail.png"></span><p>homedesign@demolink.org</p></div>
                <div class="contacts-item"><span><img src="<?php echo get_template_directory_uri(); ?>/img/phone.png"></span><p>800-2345-6789</p></div>
            </div>
            <div class="footer-item follow-block">
                <h1>Follow us</h1>
                <div class="follow-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/fb.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/tw.png">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/in.png">
                </div>
            </div>
        </div>
        <hr>
        <div class="rights">© 2019 All rights reserved.</div>
    </section>
    </body>
    </html>
    <script>
        $(document).ready(function(){
            console.log("hello world");
            $('#carousel-example-generic').on('slid.bs.carousel', function () {
                $holder = $( "ol li.active" );
                $holder.removeClass('active');
                var idx = $('div.active').index('div.item');
                $('ol.carousel-indicators li[data-slide-to="'+ idx+'"]').addClass('active');
            });

            $('ol.carousel-indicators  li').on("click",function(){
                $('ol.carousel-indicators li.active').removeClass("active");
                $(this).addClass("active");
            });
        });
        $('#carousel-example-generic').bind('mousewheel', function (e) {
            if (e.originalEvent.wheelDelta / 120 > 0) {
                $(this).carousel('next');
            }
            else {
                $(this).carousel('prev');
            }
        });
</script>
<?php
get_footer();
?>