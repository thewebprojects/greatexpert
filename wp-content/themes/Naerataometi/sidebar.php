<?php
/**
 * Created by IntelliJ IDEA.
 * User: Karine
 * Date: 19.03.2019
 * Time: 16:20
 */
?>
         <div class="slidebar">

             <?php get_search_form();
             wp_get_sidebars_widgets( $deprecated = true );
             ?>




                <h1>Links You'll Like</h1>
                <img src="<?php echo get_template_directory_uri(); ?>/img/slidebar-img.png">

                <div class="slidebar-dropdown">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">NRA Links
                            <i class="fas fa-angle-down"></i></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">test</a></li>
                            <li><a href="#">test</a></li>
                            <li><a href="#">test</a></li>
                        </ul>
                    </div>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Firearms & the Law
                            <i class="fas fa-angle-down"></i></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">test</a></li>
                            <li><a href="#">test</a></li>
                            <li><a href="#">test</a></li>
                        </ul>
                    </div>
                </div>

                <div class="newsletters">
                    <h1>Newsletters</h1>
                    <p>Leprehenderit in voluptate velit esse cillum dolore but fugiat nulla
                        pariatur. Excepteur sint occaecat </p>

                    <div class="subscribe-form">
                        <?php echo do_shortcode('[mc4wp_form id="94"]');?>
                    </div>


                </div>

                <h1>Lorem ipsum</h1>
                <img src="<?php echo get_template_directory_uri(); ?>/img/slidebar-img-2.png">

                <div class="with-us">
                    <h1>CONNECT WITH US</h1>
                    <?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>

                            <?php dynamic_sidebar( 'sidebar-1' ); ?>

                    <?php endif; ?>

                </div>


            </div>