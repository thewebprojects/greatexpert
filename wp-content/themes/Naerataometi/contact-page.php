<?php
/* Template Name: Contact*/
get_header();
$header =  get_field("header");
$content =  get_field("content");
$address =  get_field("address");
?>


<section class="single-header">
    <h2><?= $header['title']?></h2>
    <p> <?= $header['text']?></p>
</section>

<div class="single-bg">
    <div class="single-page">
        <div class="single-news contact-section">
            <h1><?=$content['title']?></h1>
            <p><?=$content['info']?></p>

            <div class="contact-adress">
                <img src="<?php echo get_template_directory_uri(); ?>/img/map-cont.png">
                <p><?=$address['ofis_address']?></p>
            </div>
            <div class="contact-adress">
                <img src="<?php echo get_template_directory_uri(); ?>/img/email_cont.png">
                <p><?=$address['ofis_email']?></p>
            </div>
            <div class="contact-adress">
                <img src="<?php echo get_template_directory_uri(); ?>/img/telephone-cont.png">
                <p><?=$address['ofis_phone']?></p>
            </div>
            <div class="clear60"></div>
            <h1>Let’s talk</h1>
            <?php echo do_shortcode('[contact-form-7 id="138" title="Contact form 1"]');?>

            <div class="clear60"></div>
            <h1>Our location</h1>

            <?php echo do_shortcode('[google_map_easy id="1"]');?>


        </div>

       <?php get_sidebar();?>
    </div>
</div>

<?php
get_footer();
?>
