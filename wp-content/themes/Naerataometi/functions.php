<?php
/**
 *  Academy functions and definitions
 *
 * @subpackage Naerataometi
 * @since 1.0.0
 */





/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */

/**
 * Enqueue scripts and styles.
 */
function imar_scripts() {
//    wp_enqueue_style( 'bootstrap-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css' );
    wp_enqueue_style( 'fontawesom-style', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css' );
//    wp_enqueue_style( 'font-awesome-style', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri(). '/vendor/bootstrap/css/bootstrap.min.css' );
//    wp_enqueue_style( 'animate-style', get_template_directory_uri(). '/libs/css/animate.css' );
//    wp_enqueue_style( 'font-awesome-style', get_template_directory_uri(). '/libs/css/font-awesome.css' );
//    wp_enqueue_style( 'owl-style', get_template_directory_uri(). '/libs/css/owl.carousel.min.css' );
    wp_enqueue_style( 'main-style', get_template_directory_uri(). '/style.css' );



    wp_enqueue_script( 'jquery-script',  get_template_directory_uri(). '/vendor/js/jquery-3.3.1.min.js' , true );
//    wp_enqueue_script( 'popper-script',  get_template_directory_uri(). '/libs/js/popper.min.js' , true );
    wp_enqueue_script( 'bootstrap-script',  get_template_directory_uri(). '/vendor/bootstrap/js/bootstrap.min.js' , true );
//    wp_enqueue_script( 'wow-script',  get_template_directory_uri(). '/libs/js/wow.min.js' , true );
    wp_enqueue_script( 'owl-script',  get_template_directory_uri(). '/vendor/js/owl.carousel.min.js' , true );
//    wp_enqueue_script( 'aos-script',  get_template_directory_uri(). '/js/aos.js' , true );
    wp_enqueue_script( 'main-script',  get_template_directory_uri(). '/js/main.js' , true );
//    wp_enqueue_script( 'jquery-script',  'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', true );
//    wp_enqueue_script( 'bootstrap-script',  'https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js', true );


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'imar_scripts' );

add_action( 'widgets_init', 'my_register_sidebars' );
function my_register_sidebars() {
    /* Register the 'primary' sidebar. */
    register_sidebar(
        array(
            'id'            => 'primary',
            'name'          => __( 'Primary Sidebar' ),
            'description'   => __( 'A short description of the sidebar.' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
    /* Repeat register_sidebar() code for additional sidebars. */
}

function wpb_widgets_init() {

    register_sidebar( array(
        'name'          => 'Custom Header Widget Area',
        'id'            => 'custom-header-widget',
        'before_widget' => '<div class="chw-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="chw-title">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'wpb_widgets_init' );


function bloggist_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'bloggist' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<section id="%1$s" class="fbox swidgets-wrap widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="sidebar-headline-wrapper"><div class="sidebarlines-wrapper"><div class="widget-title-lines"></div></div><h4 class="widget-title">',
        'after_title'   => '</h4></div>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget (1)', 'bloggist' ),
        'id'            => 'footerwidget-1',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<li id="%1$s" class="list-inline-item">',
        'after_widget'  => '</li>',
        'before_title'  => '<div class="swidget"><h3 class="widget-title">',
        'after_title'   => '</h3></div>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget (2)', 'bloggist' ),
        'id'            => 'footerwidget-2',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<li id="%1$s" class=" widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<div class="swidget" style="display: none">',
        'after_title'   => '</div>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget (3)', 'bloggist' ),
        'id'            => 'footerwidget-3',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<section id="%1$s" class="fbox widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="swidget"><h3 class="widget-title">',
        'after_title'   => '</h3></div>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget (4)', 'bloggist' ),
        'id'            => 'footerwidget-4',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<section id="%1$s" class="fbox widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="swidget"><h3 class="widget-title">',
        'after_title'   => '</h3></div>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Header Widget (1)', 'bloggist' ),
        'id'            => 'headerwidget-1',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<section id="%1$s" class="header-widget widget swidgets-wrap %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="swidget"><div class="sidebar-title-border"><h3 class="widget-title">',
        'after_title'   => '</h3></div></div>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Header Widget (2)', 'bloggist' ),
        'id'            => 'headerwidget-2',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<section id="%1$s" class="header-widget widget swidgets-wrap %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="swidget"><div class="sidebar-title-border"><h3 class="widget-title">',
        'after_title'   => '</h3></div></div>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Header Widget (3)', 'bloggist' ),
        'id'            => 'headerwidget-3',
        'description'   => esc_html__( 'Add widgets here.', 'bloggist' ),
        'before_widget' => '<section id="%1$s" class="header-widget widget swidgets-wrap %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<div class="swidget"><div class="sidebar-title-border"><h3 class="widget-title">',
        'after_title'   => '</h3></div></div>',
    ) );


}




add_action( 'widgets_init', 'bloggist_widgets_init' );




function custom_theme_features()  {

    // Add theme support for Post Formats
    add_theme_support( 'post-formats', array( 'video' ) );

    // Add theme support for Featured Images
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-thumbnails', array( 'post' ) );

    // Set custom thumbnail dimensions
    // set_post_thumbnail_size( 300, 300, true );

    // Add theme support for HTML5 Semantic Markup
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

    // Add theme support for document Title tag
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'custom_theme_features' );

function academy_unique_id( $prefix = '' ) {
    static $id_counter = 0;
    if ( function_exists( 'wp_unique_id' ) ) {
        return wp_unique_id( $prefix );
    }
    return $prefix . (string) ++$id_counter;
}

function academy_get_svg( $args = array() ) {
    // Make sure $args are an array.
    if ( empty( $args ) ) {
        return __( 'Please define default parameters in the form of an array.', 'twentyseventeen' );
    }

    // Define an icon.
    if ( false === array_key_exists( 'icon', $args ) ) {
        return __( 'Please define an SVG icon filename.', 'twentyseventeen' );
    }

    // Set defaults.
    $defaults = array(
        'icon'        => '',
        'title'       => '',
        'desc'        => '',
        'fallback'    => false,
    );

    // Parse args.
    $args = wp_parse_args( $args, $defaults );

    // Set aria hidden.
    $aria_hidden = ' aria-hidden="true"';

    // Set ARIA.
    $aria_labelledby = '';

    /*
     * Twenty Seventeen doesn't use the SVG title or description attributes; non-decorative icons are described with .screen-reader-text.
     *
     * However, child themes can use the title and description to add information to non-decorative SVG icons to improve accessibility.
     *
     * Example 1 with title: <?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right', 'title' => __( 'This is the title', 'textdomain' ) ) ); ?>
     *
     * Example 2 with title and description: <?php echo twentyseventeen_get_svg( array( 'icon' => 'arrow-right', 'title' => __( 'This is the title', 'textdomain' ), 'desc' => __( 'This is the description', 'textdomain' ) ) ); ?>
     *
     * See https://www.paciellogroup.com/blog/2013/12/using-aria-enhance-svg-accessibility/.
     */
    if ( $args['title'] ) {
        $aria_hidden     = '';
        $unique_id       = twentyseventeen_unique_id();
        $aria_labelledby = ' aria-labelledby="title-' . $unique_id . '"';

        if ( $args['desc'] ) {
            $aria_labelledby = ' aria-labelledby="title-' . $unique_id . ' desc-' . $unique_id . '"';
        }
    }

    // Begin SVG markup.
    $svg = '<svg class="icon icon-' . esc_attr( $args['icon'] ) . '"' . $aria_hidden . $aria_labelledby . ' role="img">';

    // Display the title.
    if ( $args['title'] ) {
        $svg .= '<title id="title-' . $unique_id . '">' . esc_html( $args['title'] ) . '</title>';

        // Display the desc only if the title is already set.
        if ( $args['desc'] ) {
            $svg .= '<desc id="desc-' . $unique_id . '">' . esc_html( $args['desc'] ) . '</desc>';
        }
    }

    /*
     * Display the icon.
     *
     * The whitespace around `<use>` is intentional - it is a work around to a keyboard navigation bug in Safari 10.
     *
     * See https://core.trac.wordpress.org/ticket/38387.
     */
    $svg .= ' <use href="#icon-' . esc_html( $args['icon'] ) . '" xlink:href="#icon-' . esc_html( $args['icon'] ) . '"></use> ';

    // Add some markup to use as a fallback for browsers that do not support SVGs.
    if ( $args['fallback'] ) {
        $svg .= '<span class="svg-fallback icon-' . esc_attr( $args['icon'] ) . '"></span>';
    }

    $svg .= '</svg>';

    return $svg;
}
register_nav_menus( array(
    'primary' => __( 'main menu', 'imar' ),
    'social'  => __( 'Social Links Menu', 'imar' ),
    'footer'  => __( 'Additional', 'imar' ),
) );
//function login_function()
//{
//    if ( is_user_logged_in() )
//    {
//        $user = wp_get_current_user();
////        var_dump($user->ID);
//        $customer = new WC_Customer( $user->ID );
//        $customer_last_order_id =$customer->get_last_order()->get_order_number();
//        $WC_Order_info = new WC_Order($customer_last_order_id);
//        $order_status =$WC_Order_info->get_status();
//        if($order_status == "on-hold"){
//            $completed_date = $WC_Order_info->get_date_completed();
//
//
//        }
////        $product = wc_get_product($customer_last_order_id);
//
//        $size = array_shift(  );
//        echo'<pre>';
////        var_dump(wc_get_product_terms( $customer_last_order_id));
//        echo '</pre>';
//        return ( isset( $user->ID ) ? (int) $user->ID : 0 );
//
//    }
//}
//add_action('init', 'login_function');
//function get_status($id,$from,$to,$thiss){
//    var_dump($id);
//    var_dump($from);
//    var_dump($to);
//    var_dump($thiss);
//
//}
//add_action('woocommerce_order_status_changed','get_status',10,4);
//
//add_action( 'woocommerce_payment_complete', 'so_payment_complete' );
//function so_payment_complete( $order_id ){
//    var_dump('test');
//    $order = wc_get_order( $order_id );
//    $user = $order->get_user();
//    if( $user ){
//       var_dump('test');
//    }
//}