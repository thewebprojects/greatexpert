<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( academy_unique_id( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="form-block">
        <div class="input-group">
	<input type="search" class="form-control search-field" placeholder="Search" id="<?php echo $unique_id; ?>" value="<?php echo get_search_query(); ?>" name="s" />
    <span class="input-group-btn">
    <button type="submit" class="btn search-submit"><img src="<?php echo get_template_directory_uri(); ?>/img/search%20(7).png"><span class="screen-reader-text"><?php echo _x( '', 'submit button', 'academy' ); ?></span></button>
          </span>

    </div>
</div>
</form>
